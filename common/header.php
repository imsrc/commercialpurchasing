<!DOCTYPE HTML>
<html>
    <head>
        <title>Commercial Purchasing</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="assets/css/main.css" />

        
    </head>
    <body class="homepage is-preload">
        <div id="page-wrapper">

            <!-- Header -->
            <?php
                if(basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']) == 'index.php'){
                    echo "<section id='header' class='wrapper'>";
                } elseif (basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']) == 'aboutus.php') {
                    echo "<section id='header_aboutus' class='wrapper' >";
                } elseif (basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']) == 'procurement.php') {
                    echo "<section id='header_procurement' class='wrapper'>";
                } elseif (basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']) == 'costsaving.php') {
                    echo "<section id='header_costsaving' class='wrapper'>";
                } elseif (basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']) == 'consolidation.php') {
                    echo "<section id='header_consolidation' class='wrapper'>";
                } elseif (basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']) == 'distribution.php') {
                    echo "<section id='header_distribution' class='wrapper'>";
                } elseif (basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']) == 'blog.php') {
                   echo "<section id='header_blog' class='wrapper'>";
                } elseif (basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']) == 'blogpage.php') {
                    echo "<section id='header_blog' class='wrapper'>";
                } elseif (basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']) == 'contactus.php') {
                    echo "<section id='header_contactus' class='wrapper'>";
                } else{
                    echo "<section id='header' class='wrapper'>";
                }
            ?>
            
                <!-- Logo -->
                <div id="logo">
                    <h1><a href="index.html"><img src="images/cp_logo.png" width="200" height="150"></a></h1>
                    </br></br><h1><a href="index.html">Your Procurement Specialist</a></h1>
                </div>

                <!-- Nav -->
                <nav id="nav">
                    <ul>
                        <li class="current"><a href="index.php">Home</a></li>
                        <li><a href="aboutus.php">About Us</a></li>
                        <li>
                            <a>Services</a>
                            <ul>
                                <li><a href="procurement.php">Procurement Health Check</a></li>
                                <li><a href="costsaving.php">Cost Saving</a></li>
                                <li><a href="consolidation.php">Consolidation</a></li>
                                <li><a href="distribution.php">Distribution</a></li>
                            </ul>
                        </li>
                        <li><a href="blog.php">Blog</a></li>
                        <li><a href="contactus.php">Contact Us</a></li>
                    </ul>
                </nav>

            </section>

            
