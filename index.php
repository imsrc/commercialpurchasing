<?php
    include 'config/config.php';
    include 'common/header.php';
?>

<!-- Intro -->
<section id="intro" class="wrapper style1">
    <div class="title">WHAT WE DO</div>
    <div class="container">
        <p class="style2">
            Sourcing You The Best Solution
        </p>
        <p class="style4"> A dedicated team who source a variety products and services that provide you with relevant cost savings, whilst removing the drain on your resource. </p>
    `</div>
</section>

<!-- Main -->
<section id="main" class="wrapper style2">
    <div class="title">SERVICES</div>
    <div class="container">

        <!-- Features -->
        <section id="features">
            <div class="feature-list">
                <div class="row">
                    <div class="col-6 col-12-medium">
                        <section>
                            <h3>Procurement Health Check</h3>
                            <p>Free analysis that enables you to understand how your costs fair in the current market. </p>
                        </section>
                    </div>
                    <div class="col-6 col-12-medium">
                        <section>
                            <h3>Cost Saving</h3>
                            <p> Implement cost savings that deliver on quality without the need of internal resource.  </p>
                        </section>
                    </div>
                    <div class="col-6 col-12-medium">
                        <section>
                            <h3>Logistics and Distribution</h3>
                            <p>  Ensure your distribution is operating at its strongest, whether this is setting-up a new operation, or reviewing an existing one.  </p>
                        </section>
                    </div>
                    <div class="col-6 col-12-medium">
                        <section>
                            <h3>Consolidation</h3>
                            <p> Streamline your procurement activities to maximise your buying power and simplify supply processes.   </p>
                        </section>
                    </div>
                </div>
            </div>
        </section>

    </div>
</section>

<!-- Highlights -->
<section id="highlights" class="wrapper style3">
    <div class="title">DELIVERED AS STANDARD</div>
    <div class="container">
        <div class="row aln-center">
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <h3>No costs or fees</h3>
                    <p>We will not invoice or charge you for any of the work we do.</p>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <h3>No contractual commitment</h3>
                    <p>We believe in the quality of our work, which is why we don't require a contract to be signed, and there is no obligation for you to go ahead with any of our solutions. </p>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <h3>Fully confidential</h3>
                    <p>Confidentially is important to us, which is why we never disclose your details when going to market.</p>
                </section>
            </div>
        </div>
    </div>
</section>

<!-- Categories -->
<section id="intro" class="wrapper style1">
    <div class="title">CATEGORIES</div>
    <div class="container">
        <p class="style4">We cover a broad range of products and services:</p>
        <p class="style4">Food, beverages, packaging, consumables, print, utilities and insurance, janitorial supplies, waste collection, office supplies, logistics and freight, work wear, laundry, outsource services, light equipment, and more.</p>
        <p class="style4">Our procurement solutions will save you money and deliver on quality. Whether it is for a single product or a full range. </p>
        <p class="style4">We focus on a smooth transition of any new lines or services implemented. We are there for you whenever you need us. </p>
    </div>
</section>

<!-- Footer -->
<section id="footer" class="wrapper">
    <div class="title">CONTACT US</div>
    <div class="container">
        <div class="row">
            <div class="col-6 col-12-medium">

                <!-- Contact Form -->
                <section>
                    <form method="post" action="forms/contactusForm.php">
                        <div class="row gtr-50">
                            <div class="col-6 col-12-small">
                                <input type="text" name="firstname" aria-describedby="firstName" placeholder="Firstname" id="contact-name" />
                            </div>
                            <div class="col-6 col-12-small">
                                <input type="text" name="surname" aria-describedby="surName" placeholder="Surname" id="contact-name" />
                            </div>
                            <div class="col-12 col-12-small">
                                <input type="email" name="email" aria-describedby="email" placeholder="Email" required="" id="contact-email" />
                            </div>
                            <div class="col-12">
                                <textarea name="message" id="contact-message" aria-describedby="message" placeholder="Message" rows="4"></textarea>
                                <input type="hidden" name="page" value="indexPage">
                            </div>
                            <div class="col-12">
                                <ul class="actions">
                                    <li><input type="submit" class="style1" value="Send" /></li>
                                    <li><input type="reset" class="style2" value="Reset" /></li>
                                </ul>
                                <ul class="actions">
                                    <li> 
                                    <?php
                                        if(filter_input(INPUT_GET, 'status', FILTER_SANITIZE_URL) === 'success'){
                                            echo "<br><h3 class='solid'>Message sent!</h3> ";
                                        }elseif(filter_input(INPUT_GET, 'status', FILTER_SANITIZE_URL) === 'fail'){
                                            echo "<br><h3 class='solid'>Unfortunately, something went wrong. Please try again.</h3>";
                                        }
                                    ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </section>
                
            </div>
            <div class="col-6 col-12-medium">

                <!-- Contact -->
                <section class="feature-list small">
                    <div class="row">
                        <div class="col-6 col-12-small">
                            <section>
                                <h3 class="icon solid fa-comment">Social</h3>
                                <p>
                                    <a href="https://twitter.com/c_purchasing"><img class="imageLogo" src="images/twitter.png" alt="Twitter"></a> 
                                    <a href="https://www.linkedin.com/company/3134196/"><img class="imageLogo" src="images/linkedin.jpg" alt="Linedin"></a>
                                </p>
                            </section>
                        </div>
                        <div class="col-6 col-12-small">
                            <section>
                                <h3 class="icon solid fa-phone">Phone</h3>
                                <p style="color: white">
                                    01327 842 858
                                </p>
                            </section>
                        </div>
                        <div class="col-8 col-12-small">
                            <section>
                                <h3 class="icon solid fa-envelope">Email</h3>
                                <p>
                                    <a href="mailto:info@commercial-purchasing.co.uk" target="_blank">info@commercial-purchasing.co.uk</a>
                                </p>
                            </section>
                        </div>
                    </div>
                </section>

            </div>
        </div>
        <div id="copyright">
            <ul>
                <li>Copyright © 2021 Commercial Purchasing - All Rights Reserved.</li>
            </ul>
        </div>
    </div>
</section>

</div>
<!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/browser.min.js"></script>
        <script src="assets/js/breakpoints.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>
</body>
</html>