<?php
include 'config/config.php';
include 'common/header.php';

$sql = "SELECT * FROM posts ORDER BY id DESC;";
$result = mysqli_query($conn, $sql);

// fetch all posts as an associative array called $posts
//$posts = mysqli_fetch_all($result, MYSQLI_ASSOC);

?>


<!-- Highlights -->
<section id="highlights" class="wrapper style3">
    <div class="title">Recent Articles</div>
    <div class="container">
        <?php
        //If blog page failed to load, i.e. blog doesn't exist, let the user know
        if (filter_input(INPUT_GET, 'status', FILTER_SANITIZE_URL) === 'fail') {
            echo "<h3 class='solid'>That was unexpected! Please let us know immediately so we can fire the tech support bloke!</h3> <br>";
        } 

        $count = 0;
        echo "<div class='row aln-center'>";
        while($post = mysqli_fetch_array($result)) {
			
        //foreach (array_reverse($posts) as $post):

            if ($count != 0 && $count % 3 == 0) {
                echo "</div><br><br>";
                echo "<div class='row aln-center'>";
            }
            ?>

            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <a href="blogpage.php?blogId=<?php echo $post['id']; ?>" class="image featured"><img src="images/<?php echo $post['image']; ?>" alt="" /></a>
                    <h3><?php echo $post['title'] ?></h3>
                    <p><?php echo date("F j, Y ", strtotime($post["created_at"])); ?></p>
                    <ul class="actions">
                        <li><a href="blogpage.php?blogId=<?php echo $post['id']; ?>" class="button style1">Read More</a></li>
                    </ul>
                </section>
            </div>

            <?php
            ++$count;
        //endforeach
        }
        ?>
    </div>
</section>

<!-- Footer -->
<section id="footer" class="wrapper">
    <div class="title">CONTACT US</div>
    <div class="container">

        <div id="copyright">

            <a href="https://twitter.com/c_purchasing"><img class="imageLogo" src="images/twitter.png" alt="Twitter"></a> 
            <a href="https://www.linkedin.com/company/3134196/"><img class="imageLogo" src="images/linkedin.jpg" alt="Linedin" ></a> <br>

            <ul>
                <li>Copyright © 2021 Commercial Purchasing - All Rights Reserved.</li>
            </ul>
        </div>
    </div>
</section>

</div>
<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.dropotron.min.js"></script>
<script src="assets/js/browser.min.js"></script>
<script src="assets/js/breakpoints.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>