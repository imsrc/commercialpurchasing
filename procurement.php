<?php
include 'config/config.php';
include 'common/header.php';
?>


<!-- Main -->
<section id="main" class="wrapper style2" style="text-align: center;"> 
    <div class="title">Procurement Health Check</div>
    <div class="container">
        <div class="container">
            <p class="style4"> Market trends and costs are always moving and it can be challenging to ensure your procurement activities are consistently aligned with your business’ needs, as well as continuously identifying new sources of value at the best possible cost. </p>
            <p class="style4"> Our procurement health check will empower your business to fill gaps of lost of opportunity and save you valuable time and in-house resource. We look at anything that has a cost associated to it, whether this across your whole catalogue, one category, or even just one line.  </p>
            <p class="style4"> We analyse these costs and provide you with the data that reveals areas where cost savings can be made.  </p>
            <p class="style4"> Identification of opportunities to reduce costs. Benchmarking against relevant products that match specifications if needed. Solutions that deliver on quality, as well as cost.  </p>
        </div>

    </div>
</section>

<!-- Highlights -->
<section id="highlights" class="wrapper style3" style="text-align: center;">
    <div class="title">Categories</div>
    <div class="container">
        <div class="row aln-center">
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic01.jpg" alt="" />
                    <h3>Food and beverage</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic02.jpg" alt="" />
                    <h3>Packaging</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic03.jpg" alt="" />
                    <h3>Utilities and insurance</h3>
                </section>
            </div>
        </div><br>
        <div class="row aln-center">
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic04.jpg" alt="" />
                    <h3>Office supplies and print</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic05.jpg" alt="" />
                    <h3>Laundry and waste</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic06.jpg" alt="" />
                    <h3>Chemicals and consumables</h3>
                </section>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<section id="footer" class="wrapper">
    <div class="title">CONTACT US</div>
    <div class="container">

        <div id="copyright">

            <a href="https://twitter.com/c_purchasing"><img class="imageLogo" src="images/twitter.png" alt="Twitter"></a> 
            <a href="https://www.linkedin.com/company/3134196/"><img class="imageLogo" src="images/linkedin.jpg" alt="Linedin" ></a> <br>

            <ul>
                <li>Copyright © 2021 Commercial Purchasing - All Rights Reserved.</li>
            </ul>
        </div>
    </div>
</section>

</div>
<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.dropotron.min.js"></script>
<script src="assets/js/browser.min.js"></script>
<script src="assets/js/breakpoints.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>