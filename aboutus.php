<?php
    include 'config/config.php';
    include 'common/header.php';
?>

<!-- Intro -->
<section id="intro" class="wrapper style1">
    <div class="title">WHO WE ARE</div>
    <div class="container">
        <p class="style4"> We provide a truly client focused service, specialising in reducing costs, consolidating supply chains and optimising distribution. </p>
        <p class="style4"> We are a small dedicated team of experts, focused on cost reduction. Our lean structure with low overheads allows us to provide our services without the need to charge any fees. </p>
        <p class="style4"> Our collective efforts and cooperative mindsets enable us to provide important strategic insights and a truly bespoke service to identify and implement key business benefits and cost savings. </p>
    `</div>
</section>

<!-- Highlights -->
<section id="highlights" class="wrapper style3">
    <div class="title">DELIVERED AS STANDARD</div>
    <div class="container">
        <div class="row aln-center">
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <h3>No costs or fees</h3>
                    <p>We will not invoice or charge you for any of the work we do.</p>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <h3>No contractual commitment</h3>
                    <p>We believe in the quality of our work, which is why we don't require a contract to be signed, and there is no obligation for you to go ahead with any of our solutions. </p>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <h3>Fully confidential</h3>
                    <p>Confidentially is important to us, which is why we never disclose your details when going to market.</p>
                </section>
            </div>
        </div>
    </div>
</section>

<!-- Categories -->
<section id="intro" class="wrapper style1">
    <div class="title">MANAGEMENT</div> 
    <div class="container">
        <P class="style2">Tony Bartrum, Director of Commercial Purchasing</p>
        <p class="style4">Our Director, Tony Bartrum has over 25 years’ experience in various sectors of procurement. Tony has held senior positions in blue chip organisations including Granada Purchasing (now Compass), in which he held responsibilities to oversee the activities of a team of buyers that covered all areas of food procurement with direct responsibility in the purchase of beverages and food logistical aspects within the group that included the old Forte Business, 5,000 contract catering sites and the Granada motorway sites.</p>
        <p class="style4">Following Compass, Tony was involved in the development of a consolidated chilled distribution platform delivering to 7,000 sites on a daily basis, at Peters Food Service. </p>
        <p class="style4">Tony also worked at Brakes and held the position as one of the Commercial Directors overseeing joint venture purchasing clients as well as the procurement of “not for resale” categories. </p>
        <p class="style4">Tony also spent 2 years providing consultancy services at Prestige Purchasing to hotel groups in the Caribbean and was involved developing global purchasing solutions. When he returned to the UK, Tony continued as the Commercial Director of Prestige Purchasing but changed involvement as he was one of the creators of delivering procurement and distribution solutions for many clients. </p>
        <p class="style4">It did not matter whether it was within the 230 Hotel consortium members, restaurant groups, motorway service areas, hotel groups, private hospitals, care homes or even pub groups. Needless to say, Tony Bartrum has gained extensive experience resulting in founding Commercial Purchasing. </p>
    </div>
</section>

<!-- Footer -->
<section id="footer" class="wrapper">
    <div class="title">CONTACT US</div>
    <div class="container">
       
        <div id="copyright">
            
                 <a href="https://twitter.com/c_purchasing"><img class="imageLogo" src="images/twitter.png" alt="Twitter"></a> 
                 <a href="https://www.linkedin.com/company/3134196/"><img class="imageLogo" src="images/linkedin.jpg" alt="Linedin" ></a> <br>
            
            <ul>
                <li>Copyright © 2021 Commercial Purchasing - All Rights Reserved.</li>
            </ul>
        </div>
    </div>
</section>

</div>
<!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/browser.min.js"></script>
        <script src="assets/js/breakpoints.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>
</body>
</html>