<?php
include 'config/config.php';
include 'common/header.php';

// Get the Get variable
$blogId = filter_input(INPUT_GET, "blogId") ? filter_input(INPUT_GET, "blogId") : "NULL";

if ($blogId) {
    $sql = "SELECT * FROM posts where id = '$blogId'";
    $result = mysqli_query($conn, $sql);

    // fetch all posts as an associative array called $posts
    //$blogObject = mysqli_fetch_all($result, MYSQLI_ASSOC);
} else {
    header("Location:blog.php?status=fail");
}
?>


<!-- Main -->
<section id="main" class="wrapper style2" > 
    <div class="title">Blog</div>
    <div class="container">
        <?php
            while($blog = mysqli_fetch_array($result)) {
        ?>

                <h1 style="font-size: 2em;"><?php echo $blog['title'] ?></h1><br>
                <p><?php echo date("F j, Y ", strtotime($blog["created_at"])); ?></p>

            <?php    
                // Seperate the main blog text by fullstop
                $blogText = explode(PHP_EOL, $blog['body']);
                foreach($blogText as $text):
            ?>
                    <p class="style4"> <?php echo $text ?> </p>

        <?php
                endforeach;
            }
        ?>
        <img src="images/<?php echo $blog['image']; ?>" alt="" style="width:75%"/>
                    

    </div>
</section>

<!-- Footer -->
<section id="footer" class="wrapper">
    <div class="title">CONTACT US</div>
    <div class="container">

        <div id="copyright">

            <a href="https://twitter.com/c_purchasing"><img class="imageLogo" src="images/twitter.png" alt="Twitter"></a> 
            <a href="https://www.linkedin.com/company/3134196/"><img class="imageLogo" src="images/linkedin.jpg" alt="Linedin" ></a> <br>

            <ul>
                <li>Copyright © 2021 Commercial Purchasing - All Rights Reserved.</li>
            </ul>
        </div>
    </div>
</section>

</div>
<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.dropotron.min.js"></script>
<script src="assets/js/browser.min.js"></script>
<script src="assets/js/breakpoints.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>