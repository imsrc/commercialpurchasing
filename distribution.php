<?php
include 'config/config.php';
include 'common/header.php';
?>


<!-- Main -->
<section id="main" class="wrapper style2" style="text-align: center;"> 
    <div class="title">Logistics And Distribution</div>
    <div class="container">
        <div class="container">
            <p class="style4"> Unlocking the benefits of a strong distribution arrangement is key to ensuring your business is running as efficiently and as effectively as possible.  </p>
            <p class="style4"> By optimising and streamlining your operation your business will benefit from greater product control, reduced wholesaler margins, reductions in inventory, as well as revealing greater opportunities for sourcing.   </p>
            <p class="style4"> From combining deliveries, to sourcing further up the supply chain, we will work with your business to establish the optimum distribution solution for you.  </p>
            <p class="style4"> Improve operational and process efficiency and performance. Benefit from reduced costs and tighter control. Reveal greater opportunities for sourcing at reduced margins. </p>
        </div>
    </div>
</section>
 
<!-- Highlights -->
<section id="highlights" class="wrapper style3" style="text-align: center;">
    <div class="title">Categories</div>
    <div class="container">
        <div class="row aln-center">
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic01.jpg" alt="" />
                    <h3>Food and beverage</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic02.jpg" alt="" />
                    <h3>Packaging</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic03.jpg" alt="" />
                    <h3>Utilities and insurance</h3>
                </section>
            </div>
        </div><br>
        <div class="row aln-center">
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic04.jpg" alt="" />
                    <h3>Office supplies and print</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic05.jpg" alt="" />
                    <h3>Laundry and waste</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic06.jpg" alt="" />
                    <h3>Chemicals and consumables</h3>
                </section>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<section id="footer" class="wrapper">
    <div class="title">CONTACT US</div>
    <div class="container">

        <div id="copyright">

            <a href="https://twitter.com/c_purchasing"><img class="imageLogo" src="images/twitter.png" alt="Twitter"></a> 
            <a href="https://www.linkedin.com/company/3134196/"><img class="imageLogo" src="images/linkedin.jpg" alt="Linedin" ></a> <br>

            <ul>
                <li>Copyright © 2021 Commercial Purchasing - All Rights Reserved.</li>
            </ul>
        </div>
    </div>
</section>

</div>
<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.dropotron.min.js"></script>
<script src="assets/js/browser.min.js"></script>
<script src="assets/js/breakpoints.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>