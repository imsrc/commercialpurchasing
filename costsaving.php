<?php
include 'config/config.php';
include 'common/header.php';
?>


<!-- Main -->
<section id="main" class="wrapper style2" style="text-align: center;"> 
    <div class="title">Cost Saving</div>
    <div class="container">
        <div class="container">
            <p class="style4"> It can be demanding and time consuming to consistently ensure that the products and services you buy are always at the best possible price, as well as deliver on quality. The market is always moving, and it can be difficult to know if a supplier is always providing you with their most competitive price. </p>
            <p class="style4"> Our knowledge, expertise and established relationships enable us to provide you with a range of cost saving solutions, allowing you to focus on your core business activities and objectives.  </p>
            <p class="style4"> We provide an in-depth independent analysis of data based on market research and negotiations that deliver robust and sustainable solutions, that reflect current specifications, or improve on them.  </p>
            <p class="style4"> We explore the entire market, and are not restricted to a set list of suppliers. Clear information based on initial tenders and negotiations. Little to no disruption to your operation. </p>
        </div>
    </div>
</section>

<!-- Highlights -->
<section id="highlights" class="wrapper style3" style="text-align: center;">
    <div class="title">Categories</div>
    <div class="container">
        <div class="row aln-center">
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic01.jpg" alt="" />
                    <h3>Food and beverage</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic02.jpg" alt="" />
                    <h3>Packaging</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic03.jpg" alt="" />
                    <h3>Utilities and insurance</h3>
                </section>
            </div>
        </div><br>
        <div class="row aln-center">
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic04.jpg" alt="" />
                    <h3>Office supplies and print</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic05.jpg" alt="" />
                    <h3>Laundry and waste</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic06.jpg" alt="" />
                    <h3>Chemicals and consumables</h3>
                </section>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<section id="footer" class="wrapper">
    <div class="title">CONTACT US</div>
    <div class="container">

        <div id="copyright">

            <a href="https://twitter.com/c_purchasing"><img class="imageLogo" src="images/twitter.png" alt="Twitter"></a> 
            <a href="https://www.linkedin.com/company/3134196/"><img class="imageLogo" src="images/linkedin.jpg" alt="Linedin" ></a> <br>

            <ul>
                <li>Copyright © 2021 Commercial Purchasing - All Rights Reserved.</li>
            </ul>
        </div>
    </div>
</section>

</div>
<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.dropotron.min.js"></script>
<script src="assets/js/browser.min.js"></script>
<script src="assets/js/breakpoints.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>