<?php
include 'config/config.php';
include 'common/header.php';
?>


<!-- Main -->
<section id="main" class="wrapper style2" style="text-align: center;"> 
    <div class="title">Consolidation</div>
    <div class="container">
        <div class="container">
            <p class="style4"> Consolidating your suppliers will enable your business to concentrate and simplify its buying power and reduce purchasing costs as well as save on internal process costs and strengthen its supply chain.  </p>
            <p class="style4"> The benefits of channelling procurement activities through as few suppliers as possible goes beyond cost saving. Consolidation allows your business to take back control, reducing risk, eliminate waste and expense, as well as improve overall procurement efficiency.  </p>
            <p class="style4"> Our experience with consolidation makes the process seamless and concentrates on ensuring there is as little disruption to your business during implementation.  </p>
            <p class="style4"> Initial Procurement Health Check completed to reveal areas for consolidation. Help with implementation with reduced disruption to your businesses’ operation. Identification of cost savings to be made by consolidating. </p>
        </div>
    </div>
</section>

<!-- Highlights -->
<section id="highlights" class="wrapper style3" style="text-align: center;">
    <div class="title">Categories</div>
    <div class="container">
        <div class="row aln-center">
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic01.jpg" alt="" />
                    <h3>Food and beverage</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic02.jpg" alt="" />
                    <h3>Packaging</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic03.jpg" alt="" />
                    <h3>Utilities and insurance</h3>
                </section>
            </div>
        </div><br>
        <div class="row aln-center">
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic04.jpg" alt="" />
                    <h3>Office supplies and print</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic05.jpg" alt="" />
                    <h3>Laundry and waste</h3>
                </section>
            </div>
            <div class="col-4 col-12-medium">
                <section class="highlight">
                    <img class="image featured" src="images/pic06.jpg" alt="" />
                    <h3>Chemicals and consumables</h3>
                </section>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<section id="footer" class="wrapper">
    <div class="title">CONTACT US</div>
    <div class="container">

        <div id="copyright">

            <a href="https://twitter.com/c_purchasing"><img class="imageLogo" src="images/twitter.png" alt="Twitter"></a> 
            <a href="https://www.linkedin.com/company/3134196/"><img class="imageLogo" src="images/linkedin.jpg" alt="Linedin" ></a> <br>

            <ul>
                <li>Copyright © 2021 Commercial Purchasing - All Rights Reserved.</li>
            </ul>
        </div>
    </div>
</section>

</div>
<!-- Scripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.dropotron.min.js"></script>
<script src="assets/js/browser.min.js"></script>
<script src="assets/js/breakpoints.min.js"></script>
<script src="assets/js/util.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>