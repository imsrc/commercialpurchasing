<?php
    include 'config/config.php';
    include 'common/header.php';
?>

<!-- Main -->
<section id="main" class="wrapper style2">
    <div class="title">CONTACT US</div>
        <div class="container">
        <div class="row">
            <div class="col-6 col-12-medium">

                <!-- Contact Form -->
                <section>
                    <form method="post" action="forms/contactusForm.php">
                        <div class="row gtr-50">
                            <div class="col-6 col-12-small">
                                <input type="text" name="firstname" aria-describedby="firstName" placeholder="Firstname" id="contact-name" />
                            </div>
                            <div class="col-6 col-12-small">
                                <input type="text" name="surname" aria-describedby="surName" placeholder="Surname" id="contact-name" />
                            </div>
                            <div class="col-12 col-12-small">
                                <input type="email" name="email" aria-describedby="email" placeholder="Email" required="" id="contact-email" />
                            </div>
                            <div class="col-12">
                                <textarea name="message" id="contact-message" aria-describedby="message" placeholder="Message" rows="4"></textarea>
                                <input type="hidden" name="page" value="contactUsPage">
                            </div>
                            <div class="col-12">
                                <ul class="actions">
                                    <li><input type="submit" class="style1" value="Send" /></li>
                                    <li><input type="reset" class="style2" value="Reset" /></li>
                                </ul>
                                <ul class="actions">
                                    <li> 
                                    <?php
                                        if(filter_input(INPUT_GET, 'status', FILTER_SANITIZE_URL) === 'success'){
                                            echo "<br><h3 class='solid'>Message sent!</h3> ";
                                        }elseif(filter_input(INPUT_GET, 'status', FILTER_SANITIZE_URL) === 'fail'){
                                            echo "<br><h3 class='solid'>Unfortunately, something went wrong. Please try again.</h3>";
                                        }
                                    ?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </section>
                
            </div>
            <div class="col-6 col-12-medium">

                <!-- Contact -->
                <section class="feature-list small">
                    <div class="row">
                        <div class="col-6 col-12-small">
                            <section>
                                <h3 class="icon solid fa-comment">Social</h3>
                                <p>
                                    <a href="https://twitter.com/c_purchasing"><img class="imageLogo" src="images/twitter.png" alt="Twitter"></a> 
                                    <a href="https://www.linkedin.com/company/3134196/"><img class="imageLogo" src="images/linkedin.jpg" alt="Linedin"></a>
                                </p>
                            </section>
                        </div>
                        <div class="col-6 col-12-small">
                            <section>
                                <h3 class="icon solid fa-phone">Phone</h3>
                                <p>
                                    01630 673 905
                                </p>
                            </section>
                        </div>
                        <div class="col-8 col-12-small">
                            <section>
                                <h3 class="icon solid fa-envelope">Email</h3>
                                <p>
                                    <a href="mailto:info@commercial-purchasing.co.uk" target="_blank">info@commercial-purchasing.co.uk</a>
                                </p>
                            </section>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<section id="footer" class="wrapper">
    <div class="title">CONTACT US</div>
    <div class="container">
        <div id="copyright">
            <ul>
                <li>Copyright © 2021 Commercial Purchasing - All Rights Reserved.</li>
            </ul>
        </div>
    </div>
</section>

</div>
<!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/browser.min.js"></script>
        <script src="assets/js/breakpoints.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>
</body>
</html>